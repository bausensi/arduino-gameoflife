/*
 * Conway's Game of Life: For Atmega328p enabled devices
 * connected to an I2C driver for HD44780 LCD character displays.
 * Written in Arduino.
 * 2019 Benjamin Ausensi. Public domain
 */

#include <LiquidCrystal_I2C.h>

//Custom characters to multiplex every LCD character into two
uint8_t upper_half[8] = {
  0b11111,
  0b11111,
  0b11111,
  0b11111,
  0b00000,
  0b00000,
  0b00000,
  0b00000,
};

uint8_t lower_half[8] = {
  0b00000,
  0b00000,
  0b00000,
  0b00000,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
};

//Convenience macros: collapsed for loops
#define FOR_XY     for(uint8_t x = 0; x < field_x; x++) for(uint8_t y = 0; y < field_y; y++)

//Editable field parameters
const uint8_t   field_x             = 20;   //The horizontal size of the playfield
const uint8_t   field_y             = 8;    //The vertical size of the playfield
const uint16_t  GENERATION_TIME_MS = 100;   //How much time in milliseconds each generation of cells lasts.

LiquidCrystal_I2C lcd(0x27, 20, 4);
uint8_t playfield_present[field_y][field_x];

void setup() {
  memset(&playfield_present, 0, sizeof(playfield_present));

  lcd.init();
  lcd.createChar(0, upper_half);
  lcd.createChar(1, lower_half);
  
  _delay_ms(100);
  lcd.backlight();
  lcd.setCursor(2, 0);
  lcd.print("Conway's Game of");
  lcd.setCursor(0, 1);
  lcd.print("Life Arduino Edition");
  lcd.setCursor(8, 3);
  lcd.print("v1.0");
  _delay_ms(5000);

  //Let's start with a glider.
  playfield_present[5][8]  = 1;
  playfield_present[5][9]  = 1;
  playfield_present[5][10] = 1;
  playfield_present[4][10] = 1;
  playfield_present[3][9]  = 1;
}

void loop() {
  //each cell of the display has two characters.
  //(a, b.h)
  
  FOR_XY {
    if(y & 1) {
      lcd.setCursor(x, y);
      if(playfield_present[y][x])
        lcd.write((playfield_present[y - 1][x]) ? '\xFF' : 1);
      else
        lcd.write((playfield_present[y - 1][x]) ? 0 : ' ');
    }
    else {
      lcd.setCursor(x, (y / 2));
      if(playfield_present[y][x])
        lcd.write((playfield_present[y + 1][x]) ? '\xFF' : 0);
      else
        lcd.write((playfield_present[y + 1][x]) ? 1 : ' ');
    }
  }
    
  uint8_t playfield_future[field_y][field_x];
  memset(&playfield_future, 0, sizeof(playfield_future));
  
  FOR_XY {
    //TODO: use a cleaner solution
    uint8_t current_neighbors = 0;
    if(playfield_present[(y + field_y - 1) % field_y][x]) current_neighbors++;
    if(playfield_present[(y + field_y + 1) % field_y][x]) current_neighbors++;
    if(playfield_present[y][(x + field_x - 1) % field_x]) current_neighbors++;
    if(playfield_present[y][(x + field_x + 1) % field_x]) current_neighbors++;
      
    if(playfield_present[(y + field_y - 1) % field_y][(x + field_x - 1) % field_x]) current_neighbors++;
    if(playfield_present[(y + field_y - 1) % field_y][(x + field_x + 1) % field_x]) current_neighbors++;
    if(playfield_present[(y + field_y + 1) % field_y][(x + field_x - 1) % field_x]) current_neighbors++;
    if(playfield_present[(y + field_y + 1) % field_y][(x + field_x + 1) % field_x]) current_neighbors++;

    //Stasis:
    if(current_neighbors == 2)
      playfield_future[y][x] = playfield_present[y][x];
      
    //Growth:
    if(current_neighbors == 3)
      playfield_future[y][x] = 1;
      
    //Death:
    if(current_neighbors < 2 || current_neighbors > 3)
      playfield_future[y][x] = 0;
  }
  memcpy(&playfield_present, &playfield_future, sizeof(playfield_present));
  _delay_ms(GENERATION_TIME_MS);
}